DESTFILE ?= dist/mailsave.py

PHONY: all lint

all:
	mkdir -p dist
	# Add shebang
	echo '#!/usr/bin/env python3' > $(DESTFILE)
	# Concatenate application files
	cat mailsave/__init__.py mailsave/smtp.py mailsave/cli.py >> $(DESTFILE)
	# Remove import from mailsave
	sed -i '/from mailsave/d' $(DESTFILE)
	# Make it executable
	chmod +x $(DESTFILE)

lint:
	python3-flake8 --max-line-length 99 --exclude .svn,CVS,.bzr,.hg,.git,__pycache__,.tox,.eggs,*.egg,build,dist

test: all
	pytest-3 tests
