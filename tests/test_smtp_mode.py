import os
import subprocess
import shutil

from copy import deepcopy
from email import message_from_string
from email.mime.text import MIMEText
from mailbox import mboxMessage
from os import path
from smtplib import SMTP
from time import sleep

import pytest


TEST_OUTPUT_FOLDER = '/tmp/mailsave'
FILE_NAME = '{to}.mbox'
CMD = [
    './dist/mailsave.py',
    '--dir', TEST_OUTPUT_FOLDER,
    '--filename', FILE_NAME,
    '--server'
]
SAVED_FILE_NAME = 'jujens+mailsave@jujens.eu.mbox'
SAVED_FILE_PATH = path.join(TEST_OUTPUT_FOLDER, SAVED_FILE_NAME)


@pytest.fixture(autouse=True)
def setup_save_dir():
    if path.exists(TEST_OUTPUT_FOLDER):
        shutil.rmtree(TEST_OUTPUT_FOLDER)

    os.makedirs(TEST_OUTPUT_FOLDER)


def run(mail, command=None):
    if command is None:
        command = CMD

    p = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    sleep(1)

    if 'Subject: Test HTML email' in mail:
        subtype = 'html'
    else:
        subtype = 'plain'

    mbox = mboxMessage(message=mail)

    payload = mbox.get_payload()
    if not isinstance(payload, str):
        if payload[1]['Content-Type'].startswith('text/html;'):
            payload = payload[1].get_payload()
        else:
            payload = payload[0].get_payload()

    msg = MIMEText(payload, subtype)
    msg['Subject'] = mbox['Subject']
    msg['From'] = mbox['From']
    msg['To'] = mbox['To']

    conn = SMTP('localhost', 2525)
    conn.set_debuglevel(False)
    conn.sendmail(msg['From'], [mbox['To']], msg.as_string())
    conn.quit()

    p.terminate()
    p.wait()

    return p.stdout.readlines(), p.stderr.readlines(), p.returncode


def verify(stdout, stderr, return_code, mail):
    assert stdout == []
    assert len(stderr) == 2
    assert stderr[0] == b'INFO:root:Listening for mail on 127.0.0.1:2525\n'
    assert stderr[1].startswith(b'INFO:root:Received mail from address')
    assert return_code == -15

    received_email = message_from_string(mail)

    with open(SAVED_FILE_PATH) as result_file:
        expected_message = message_from_string(result_file.read())

    received_payload = received_email.get_payload()
    expected_payload = expected_message.get_payload()

    if isinstance(received_payload, str):
        assert received_payload == expected_payload
        # If email has attachment, the boundary, stored in this header, may vary.
        assert received_email['Content-Type'].lower() == expected_message['Content-Type'].lower()
    else:
        if received_payload[1]['Content-Type'].startswith('text/html;'):
            assert received_payload[1].get_payload() == expected_payload
        else:
            received_payload[0].get_payload() == expected_payload

    assert received_email['Subject'] == expected_message['Subject']
    assert received_email['To'] == expected_message['To']
    assert received_email['From'] == expected_message['From']


def test_save_plain_text():
    with open('tests/data/plain_text.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_save_plain_text_signed():
    with open('tests/data/plain_text_signed.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_save_html_message():
    with open('tests/data/html_message.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_no_save():
    with open('tests/data/plain_text.mbox') as mail_file:
        mail = mail_file.read()

    cmd = deepcopy(CMD)
    cmd.append('--no-save')

    run(mail, command=cmd)

    assert not path.exists(SAVED_FILE_PATH)


def test_save_html_text_alternative():
    with open('tests/data/html_message.mbox') as mail_file:
        mail = mail_file.read()

    cmd = deepcopy(CMD)
    cmd.append('--extract-html')

    stdout, stderr, _ = run(mail, command=cmd)

    assert stdout == []
    assert len(stderr) == 2

    assert path.exists(SAVED_FILE_PATH.replace('.mbox', '.html'))
    with open(SAVED_FILE_PATH.replace('.mbox', '.html')) as html_file:
        assert html_file.read().startswith('<html><head></head><body><h1>A title</h1>')
