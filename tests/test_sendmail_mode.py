import os
import subprocess
import shutil

from copy import deepcopy
from datetime import datetime
from email import message_from_string
from os import path

import pytest


TEST_OUTPUT_FOLDER = '/tmp/mailsave'
FILE_NAME = '{to}.mbox'
CMD = [
    './dist/mailsave.py',
    '--dir', TEST_OUTPUT_FOLDER,
    '--filename', FILE_NAME,
]
SAVED_FILE_NAME = 'jujens+mailsave@jujens.eu.mbox'
SAVED_FILE_PATH = path.join(TEST_OUTPUT_FOLDER, SAVED_FILE_NAME)


@pytest.fixture(autouse=True)
def setup_save_dir():
    if path.exists(TEST_OUTPUT_FOLDER):
        shutil.rmtree(TEST_OUTPUT_FOLDER)

    os.makedirs(TEST_OUTPUT_FOLDER)


def run(mail, command=None):
    if command is None:
        command = CMD

    p = subprocess.Popen(
        command,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = p.communicate(input=mail.encode('utf-8'))

    return stdout, stderr, p.returncode


def verify(stdout, stderr, return_code, mail):
    assert stdout == b''
    assert stderr == b''
    assert return_code == 0

    received_email = message_from_string(mail)

    with open(SAVED_FILE_PATH) as result_file:
        expected_message = message_from_string(result_file.read())

    received_payload = received_email.get_payload()
    expected_payload = expected_message.get_payload()

    if isinstance(received_payload, str):
        assert received_payload == expected_payload
        # If email has attachment, the boundary, stored in this header, may vary.
        assert received_email['Content-Type'].lower() == expected_message['Content-Type'].lower()
    else:
        for i, p in enumerate(received_payload):
            assert p.get_payload() == expected_payload[i].get_payload()

    assert received_email['Subject'] == expected_message['Subject']
    assert received_email['To'] == expected_message['To']
    assert received_email['From'] == expected_message['From']


def test_save_plain_text():
    with open('tests/data/plain_text.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_save_plain_text_signed():
    with open('tests/data/plain_text_signed.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_save_html_message():
    with open('tests/data/html_message.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_output_name():
    with open('tests/data/plain_text.mbox') as mail_file:
        mail = mail_file.read()

    file_name = '{date}__{to}__{from_}__{subject}.mbox'
    cmd = [
        './dist/mailsave.py',
        '--dir', TEST_OUTPUT_FOLDER,
        '--filename', file_name,
    ]
    date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    saved_file_name = (f'{date}__jujens+mailsave@jujens.eu__'
                       f'example@example.com__'
                       f'my-test-email.mbox')

    run(mail, command=cmd)

    assert path.exists(path.join(TEST_OUTPUT_FOLDER, saved_file_name))


def test_no_save():
    with open('tests/data/plain_text.mbox') as mail_file:
        mail = mail_file.read()

    cmd = deepcopy(CMD)
    cmd.append('--no-save')

    run(mail, command=cmd)

    assert not path.exists(SAVED_FILE_PATH)


def test_save_html_text_alternative():
    with open('tests/data/html_message.mbox') as mail_file:
        mail = mail_file.read()

    cmd = deepcopy(CMD)
    cmd.append('--extract-html')

    stdout, stderr, return_code = run(mail, command=cmd)

    assert stdout == b''
    assert stderr == b''
    assert return_code == 0

    assert path.exists(SAVED_FILE_PATH.replace('.mbox', '.html'))
    with open(SAVED_FILE_PATH.replace('.mbox', '.html')) as html_file:
        assert html_file.read().startswith('<html><head></head><body><h1>A title</h1>')


def test_save_html_no_text__alternative():
    with open('tests/data/html_message_no_text_alternative.mbox') as mail_file:
        mail = mail_file.read()

    cmd = deepcopy(CMD)
    cmd.append('--extract-html')

    stdout, stderr, return_code = run(mail, command=cmd)

    assert stdout == b''
    assert stderr == b''
    assert return_code == 0

    assert path.exists(SAVED_FILE_PATH.replace('.mbox', '.html'))
    with open(SAVED_FILE_PATH.replace('.mbox', '.html')) as html_file:
        assert html_file.read().startswith('<html><head></head><body><h1>A title</h1>')


def test_save_html_t_opt():
    with open('tests/data/html_message_dot_encoded.mbox') as mail_file:
        mail = mail_file.read()

    with open('tests/data/html_message_dot_decoded.mbox') as result_mail_file:
        result_mail = message_from_string(result_mail_file.read())

    cmd = deepcopy(CMD)
    cmd.append('-t')

    stdout, stderr, return_code = run(mail, command=cmd)

    assert stdout == b''
    assert stderr == b''
    assert return_code == 0

    with open(SAVED_FILE_PATH) as mbox_file:
        mbox_mail = message_from_string(mbox_file.read())
        assert mbox_mail.get_payload() == result_mail.get_payload()


def test_save_html_ti_opt():
    with open('tests/data/html_message_dot_encoded.mbox') as mail_file:
        mail = mail_file.read()

    with open('tests/data/html_message_dot_encoded.mbox') as result_mail_file:
        result_mail = message_from_string(result_mail_file.read())

    cmd = deepcopy(CMD)
    cmd.append('-ti')

    stdout, stderr, return_code = run(mail, command=cmd)

    assert stdout == b''
    assert stderr == b''
    assert return_code == 0

    with open(SAVED_FILE_PATH) as mbox_file:
        mbox_mail = message_from_string(mbox_file.read())
        assert mbox_mail.get_payload() == result_mail.get_payload()
