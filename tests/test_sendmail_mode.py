import os
import subprocess
import shutil

from email import message_from_string
from os import path

import pytest


TEST_OUTPUT_FOLDER = '/tmp/mailsave'
FILE_NAME = '{to}.mbox'
CMD = [
    './dist/mailsave.py',
    '--dir', TEST_OUTPUT_FOLDER,
    '--filename', FILE_NAME,
]
SAVED_FILE_NAME = 'jujens+mailsave@jujens.eu.mbox'
SAVED_FILE_PATH = path.join(TEST_OUTPUT_FOLDER, SAVED_FILE_NAME)


@pytest.fixture(autouse=True)
def setup_save_dir():
    if path.exists(TEST_OUTPUT_FOLDER):
        shutil.rmtree(TEST_OUTPUT_FOLDER)

    os.makedirs(TEST_OUTPUT_FOLDER)


def run(mail):
    p = subprocess.Popen(
        CMD,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = p.communicate(input=mail.encode('utf-8'))

    return stdout, stderr, p.returncode


def verify(stdout, stderr, return_code, mail):
    assert stdout == b''
    assert stderr == b''
    assert return_code == 0

    received_email = message_from_string(mail)

    with open(SAVED_FILE_PATH) as result_file:
        expected_message = message_from_string(result_file.read())

    received_payload = received_email.get_payload()
    expected_payload = expected_message.get_payload()

    if isinstance(received_payload, str):
        assert received_payload == expected_payload
        # If email has attachment, the boundary, stored in this header, may vary.
        assert received_email['Content-Type'].lower() == expected_message['Content-Type'].lower()
    else:
        for i, p in enumerate(received_payload):
            assert p.get_payload() == expected_payload[i].get_payload()

    assert received_email['Subject'] == expected_message['Subject']
    assert received_email['To'] == expected_message['To']
    assert received_email['From'] == expected_message['From']


def test_save_plain_text():
    with open('tests/data/plain_text.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_save_plain_text_signed():
    with open('tests/data/plain_text_signed.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)


def test_save_html_message():
    with open('tests/data/html_message.mbox') as mail_file:
        mail = mail_file.read()

    stdout, stderr, return_code = run(mail)

    verify(stdout, stderr, return_code, mail)
