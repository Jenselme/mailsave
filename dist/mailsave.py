#!/usr/bin/env python3
import os
import sys

from datetime import datetime
from mailbox import mboxMessage
from os.path import join


__version__ = '0.5.0'


def save(content, folder='.', filename=None):
    mbox = mboxMessage(message=content)
    to = mbox['to']
    from_ = mbox.get_from()

    try:
        os.makedirs(folder, exist_ok=True)
    except FileExistsError:
        print('{} is a file'.format(folder), file=sys.stderr)
    else:
        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_name = filename or '{date}.mbox'
        file_name = file_name.format(date=date, to=to, from_=from_)
        file_path = join(folder, file_name)
        mode = 'wb' if isinstance(content, bytes) else 'w'

        with open(file_path, mode) as output:
            output.write(content)
import logging

from smtpd import SMTPServer


class MailSaveServer(SMTPServer):
    def __init__(self, host, port, dir, filename=None):
        logging.basicConfig(level=logging.INFO)
        super().__init__((host, port), None)
        self.dir = dir
        self.filename = filename
        logging.info('Listening for mail on {}:{}'.format(host, port))

    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        logging.info('Received mail from address {} from peer {}'.format(mailfrom, peer))
        save(data, folder=self.dir, filename=self.filename)
#!/usr/bin/env python3

import argparse
import asyncore
import sys

from io import StringIO


def main():
    parser = argparse.ArgumentParser(
        description='Dump mails into files. Can be used as a '
                    'replacement for sendmail or an SMTP server'
    )
    parser.add_argument(
        '--dir',
        default='.',
        help='Directory in which to save the mails.',
    )
    parser.add_argument(
        '--filename',
        default=None,
        help='Directory in which to save the mails. If not given, it will use the date '
             'like 2017-07-07_14-78-00.mbox You can use in your filename:'
             '{date} that will be replaced by the date, '
             '{from_} that will be replaced by the from address, '
             '{to} that will be replaced by the to address',
    )
    parser.add_argument(
        '--server',
        action='store_true',
        default=False,
        help='Listen on a TPC socket for mails.',
    )
    parser.add_argument(
        '--port',
        type=int,
        default=2525,
        help='Port on which to listen. For server mode.',
    )
    parser.add_argument(
        '--host',
        type=str,
        default='127.0.0.1',
        help='Host on which to listen. For server mode. Can be a hostnane like localhost '
             'or an ip address',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='store_true',
        help='Print the version and exit',
    )
    args, _ = parser.parse_known_args()
    if args.version:
        print(__version__)
    elif args.server:
        read_server(args)
    else:
        read_stdin(args)


def read_server(args):
    server = MailSaveServer(args.host, args.port, args.dir, filename=args.filename)  # noqa
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        print('Quitting')


def read_stdin(args):
    content = StringIO()
    try:
        for line in iter(sys.stdin.readline, ''):
            content.write(line)

        save(content.getvalue(), folder=args.dir, filename=args.filename)
    except KeyboardInterrupt:
        print('Quitting')


if __name__ == '__main__':
    main()
