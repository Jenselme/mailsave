#!/usr/bin/env python3
import base64
import os
import re
import sys
import unicodedata

from datetime import datetime
from email import (
    charset as Charset,
    message_from_bytes,
    message_from_string,
)
from os.path import join


# Taken from https://github.com/django/django/blob/458e2fbfcc0a06d7d55ff5a1dcd79c91c64e8138/django/core/mail/message.py  # noqa
RFC5322_EMAIL_LINE_LENGTH_LIMIT = 998

#  From https://github.com/django/django/blob/458e2fbfcc0a06d7d55ff5a1dcd79c91c64e8138/django/core/mail/message.py#L24  # noqa
utf8_charset = Charset.Charset('utf-8')
utf8_charset.body_encoding = None  # Python defaults to BASE64
utf8_charset_qp = Charset.Charset('utf-8')
utf8_charset_qp.body_encoding = Charset.QP


__version__ = '0.8.0'


def save(content, folder='.', filename=None, extract_html=False):
    if isinstance(content, str):
        message = message_from_string(content)
    else:
        message = message_from_bytes(content)

    charset = utf8_charset
    if message['Content-Transfer-Encoding'] == 'base64':
        content = base64.b64decode(message.get_payload())
        # From https://github.com/django/django/blob/458e2fbfcc0a06d7d55ff5a1dcd79c91c64e8138/django/core/mail/message.py#L220  # noqa

        if any(len(l) > RFC5322_EMAIL_LINE_LENGTH_LIMIT for l in content.splitlines()):
            charset = utf8_charset_qp
        message.set_payload(content, charset=charset)

    message.set_charset(charset)

    to = message.get('To', None)
    from_ = message.get('from', None)
    subject = message.get('subject', None)

    try:
        os.makedirs(folder, exist_ok=True)
    except FileExistsError:
        print('{} is a file'.format(folder), file=sys.stderr)
    else:
        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        file_name = filename or '{date}.mbox'
        file_name = file_name.format(date=date, to=to, from_=from_, subject=subject)
        file_name = normalize_file_name(file_name)
        file_path = join(folder, file_name)

        with open(file_path, 'wb') as output:
            output.write(message.as_bytes())

    if extract_html:
        extract_html_from_message(message, file_path.replace('.mbox', '.html'))


def extract_html_from_message(message, file_name):
    payload = message.get_payload()
    if isinstance(payload, str) and is_part_html(message):
        save_html(payload, file_name)
    else:
        for part in message.walk():
            payload = part.get_payload(decode=True)
            if isinstance(payload, bytes) and is_part_html(part):
                save_html(payload, file_name)
                break


def is_part_html(part):
    return part['Content-Type'].startswith('text/html;')


def save_html(content, file_name):
    mode = 'wb' if isinstance(content, bytes) else 'w'
    with open(file_name, mode) as output:
        output.write(content)


def normalize_file_name(file_name):
    # From http://stackoverflow.com/a/295466
    file_name = unicodedata.normalize('NFKD', file_name)\
        .encode('ascii', 'ignore')\
        .decode('ascii')
    file_name = re.sub('[^\w\s\-\.@<>+]', '', file_name).strip().lower()
    return re.sub('[-\s]+', '-', file_name)
import logging

from smtpd import SMTPServer


class MailSaveServer(SMTPServer):
    def __init__(self, host, port, dir, filename=None, save=True, extract_html=False):
        logging.basicConfig(level=logging.INFO)
        super().__init__((host, port), None)
        self.dir = dir
        self.filename = filename
        self.save = save
        self.extract_html = extract_html
        logging.info('Listening for mail on {}:{}'.format(host, port))

    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        logging.info('Received mail from address {} from peer {}'.format(mailfrom, peer))
        if self.save:
            save(data, folder=self.dir, filename=self.filename, extract_html=self.extract_html)
#!/usr/bin/env python3

import argparse
import asyncore
import re
import sys

from copy import deepcopy
from io import BytesIO


def main():
    parser = argparse.ArgumentParser(
        description='Dump mails into files. Can be used as a '
                    'replacement for sendmail or an SMTP server'
    )
    parser.add_argument(
        '--dir',
        default='.',
        help='Directory in which to save the mails.',
    )
    parser.add_argument(
        '--filename',
        default=None,
        help='Directory in which to save the mails. If not given, it will use the date '
             'like 2017-07-07_14-78-00.mbox You can use in your filename:'
             '{date} that will be replaced by the date, '
             '{from_} that will be replaced by the from address, '
             '{to} that will be replaced by the to address',
    )
    parser.add_argument(
        '-t',
        action='store_true',
        help='Unescape dots located at the start of a line. For instance, swiftmailer '
             'will espape dots at the start of a line (ie replace ``.`` by ``..``) '
             'before transferring the mail to sendmail because this is how sendmail '
             'expects to receive a mail. This allows mailsave to act as a drop in '
             'replacement for ``sendmail -t``'
    )
    parser.add_argument(
        '--no-save',
        dest='save',
        action='store_false',
        help='Do not save the email in the output folder. Usefull if you need something '
             'to act like sendmail but do not care about the email.'
    )
    parser.add_argument(
        '--extract-html',
        action='store_true',
        help='If the body of the message is in HTML or if it has an attachement in HTMl, '
             'extract it in a dedicated file. The file will be named after the mbox file. '
             'Only the extension will change'
    )
    parser.add_argument(
        '--server',
        action='store_true',
        default=False,
        help='Listen on a TPC socket for mails.',
    )
    parser.add_argument(
        '--port',
        type=int,
        default=2525,
        help='Port on which to listen. For server mode.',
    )
    parser.add_argument(
        '--host',
        type=str,
        default='127.0.0.1',
        help='Host on which to listen. For server mode. Can be a hostnane like localhost '
             'or an ip address',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='store_true',
        help='Print the version and exit',
    )

    # If -i or -ti is used, remove the -t option.
    args = deepcopy(sys.argv)
    ti_option = {'-i', '-it', '-ti'}
    opt_to_remove = set(args) & ti_option
    if len(opt_to_remove) > 0:
        args = [arg for arg in args if arg not in opt_to_remove]

    args, _ = parser.parse_known_args(args=args)

    if args.version:
        print(__version__)
    elif args.server:
        read_server(args)
    else:
        read_stdin(args)


def read_server(args):
    server = MailSaveServer(  # noqa
        args.host,
        args.port,
        args.dir,
        filename=args.filename,
        extract_html=args.extract_html,
        save=args.save
    )
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        print('Quitting')


def read_stdin(args):
    content = BytesIO()
    try:
        for line in iter(sys.stdin.buffer.read, b''):
            content.write(line)

        content = content.getvalue()
        if args.t:
            content = re.sub(b'\n\.\.', b'\n.', content, re.M)

        if args.save:
            save(
                content,
                folder=args.dir,
                filename=args.filename,
                extract_html=args.extract_html
            )
    except KeyboardInterrupt:
        print('Quitting')


if __name__ == '__main__':
    main()
